/*
 * Name: Lucas Connors
 * Project: Dutch Blitz Sidekick
 * File: Model.java
 */

package com.revolutiontech.dutchblitz;

import android.util.Log;
import java.util.Observable;
import java.util.Observer;

public class Model extends Observable {
	// constants
	private static final int P_GREEN = 0;
	/* not used yet
	private static final int P_RED = 1;
	private static final int P_BLUE = 2;
	private static final int P_YELLOW = 3;
	*/
	
	// model data
	private int currentPlayer;
	private Boolean playerActive[] = new Boolean[4];
	private int playerPlace[] = new int[4];
	private int playerScore[] = new int[4];
	private int playerDutchPile[] = new int[4];
	private int playerBlitzPile[] = new int[4];
	private int playerRound[] = new int[4];
	private boolean calculated[] = new boolean[4];
	
	// constructor
	Model(){
		// init values
		reset();
	}
	
	// getters
	public int getCurrentPlayer(){
		return currentPlayer;
	}
	public Boolean getPlayerActive (int player){
		return playerActive[player];
	}
	public int getPlayerPlace(int player){
		return playerPlace[player];
	}
	public int getPlayerScore(int player){
		return playerScore[player];
	}
	public int getPlayerRound(int player){
		return playerRound[player];
	}
	public int getMinPlayerRound(){
		int min = -1;
		for (int i=0; i<4; ++i){
			if (playerActive[i]){
				int thisRound = getPlayerRound(i);
				if (thisRound < min || min == -1){
					min = thisRound;
				}
			}
		}
		return min;
	}
	public int getPlayerDutchPile(){
		return playerDutchPile[currentPlayer];
	}
	public int getPlayerBlitzPile(){
		return playerBlitzPile[currentPlayer];
	}
	public boolean getCalculated(){
		return calculated[currentPlayer];
	}
	public int getPlayerRoundScore(){
		return playerDutchPile[currentPlayer] - 2*playerBlitzPile[currentPlayer];
	}
	
	// setters
	public void setCurrentPlayer(int player){
		if (playerActive[player]){
			currentPlayer = player;
		}
	}
	public void setPlayerActive(int player, Boolean bool){
		if (bool && !playerActive[player]){		// when a player is joining in, bring him to the right round
			setPlayerRoundToMin(player);
		}
		playerActive[player] = bool;
	}
	public void setPlayerDutchPile(int dutch){
		if (playerActive[currentPlayer]){
			if (dutch > 40){					// could not have more than 40 cards in Dutch Pile
				playerDutchPile[currentPlayer] = 40;
			}
			else {
				playerDutchPile[currentPlayer] = dutch;
			}
			setChanged();
			notifyObservers();
		}
	}
	public void setPlayerBlitzPile(int blitz){
		if (playerActive[currentPlayer]){
			if (blitz > 10){					// could not have more than 10 cards in Blitz Pile
				playerBlitzPile[currentPlayer] = 10;
			}
			else {
				playerBlitzPile[currentPlayer] = blitz;
			}
			setChanged();
			notifyObservers();
		}
	}
	public void setCalculated(boolean bool){
		if (playerActive[currentPlayer]){
			calculated[currentPlayer] = bool;
		}
	}
	
	// calculates the player's score for this round and adds it to their current score
	public void calculatePlayerRoundScore(){
		if (playerActive[currentPlayer]){
			playerScore[currentPlayer] += getPlayerRoundScore();
		}
	}
	// increase the current player's round
	public void increasePlayerRound(){
		if (playerActive[currentPlayer]){
			++playerRound[currentPlayer];
		}
	}
	// sets an arriving player to the minimum round
	public void setPlayerRoundToMin(int player){
		int min = -1;
		for (int i=0; i<4; ++i){
			if (playerActive[i] && i != player){
				int thisRound = getPlayerRound(i);
				if (thisRound < min || min == -1){
					min = thisRound;
				}
			}
		}
		playerRound[player] = min;
	}
	// calculates player places
	public void calculatePlayerPlaces(){
		for (int i=0; i<4; ++i){
			if (!playerActive[i]){
				playerPlace[i] = -1;
			}
			else {
				int place = 1;
				for (int j=0; j<4; ++j){
					if (i != j && playerActive[j] && playerScore[i] <= playerScore[j]){
						++place;
					}
				}
				playerPlace[i] = place;
			}
		}
	}
	
	// reset model
	public void reset(){
		currentPlayer = P_GREEN;
		for (int i=0; i<4; ++i){
			playerActive[i] = true;
			playerPlace[i] = 4;
			playerScore[i] = 0;
			playerDutchPile[i] = 0;
			playerBlitzPile[i] = 0;
			playerRound[i] = 1;
			calculated[i] = false;
		}
		setChanged();
		notifyObservers();
	}
	
	// Observer methods
	@Override
	public void addObserver(Observer observer) {
		Log.d("Dutch Blitz", "Model: Observer added");
		super.addObserver(observer);
	}

	@Override
	public synchronized void deleteObservers() {
		super.deleteObservers();
	}

	@Override
	public void notifyObservers() {
		Log.d("Dutch Blitz", "Model: Observers notified");
		super.notifyObservers();
	}

	@Override
	protected void setChanged() {
		super.setChanged();
	}

	@Override
	protected void clearChanged() {
		super.clearChanged();
	}
}