/*
 * Name: Lucas Connors
 * Project: Dutch Blitz Sidekick
 * File: PlayerView.java
 */

package com.revolutiontech.dutchblitz;

import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.content.Intent;
import android.util.*;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Button;

public class PlayerView extends RelativeLayout implements Observer {
	// constants
	private static final int P_GREEN = 0;
	private static final int P_RED = 1;
	private static final int P_BLUE = 2;
	private static final int P_YELLOW = 3;
	
	// player
	private int player;
	
	// widgets
	private TextView txtPlace;
	private TextView txtScore;
	private Button btnGoScore;
	
	// constructor
	public PlayerView(Context context, int player){
		super(context);
		
		Log.d("Dutch Blitz", "PlayerView constructor");
		
		// save player
		this.player = player;
		
		// inflate the view into the display
		int resPlayerView;
		switch (player){
			case P_GREEN:
				resPlayerView = R.layout.player_view_green;
				break;
			case P_RED:
				resPlayerView = R.layout.player_view_red;
				break;
			case P_BLUE:
				resPlayerView = R.layout.player_view_blue;
				break;
			case P_YELLOW:
				resPlayerView = R.layout.player_view_yellow;
				break;
			default:
				Log.d("Dutch Blitz", "Error: Player does not exist.");
				resPlayerView = -1;
				break;
		}
		View.inflate(context, resPlayerView, this);
		
		// add this to model's observers
		MyApplication.model.addObserver(this);
		
		// get widgets
		int resTxtPlace;
		int resTxtScore;
		int resBtnGoScore;
		switch (player){
			case P_GREEN:
				resTxtPlace = R.id.txtPlaceGreen;
				resTxtScore = R.id.txtScoreGreen;
				resBtnGoScore = R.id.btnGoScoreGreen;
				break;
			case P_RED:
				resTxtPlace = R.id.txtPlaceRed;
				resTxtScore = R.id.txtScoreRed;
				resBtnGoScore = R.id.btnGoScoreRed;
				break;
			case P_BLUE:
				resTxtPlace = R.id.txtPlaceBlue;
				resTxtScore = R.id.txtScoreBlue;
				resBtnGoScore = R.id.btnGoScoreBlue;
				break;
			case P_YELLOW:
				resTxtPlace = R.id.txtPlaceYellow;
				resTxtScore = R.id.txtScoreYellow;
				resBtnGoScore = R.id.btnGoScoreYellow;
				break;
			default:
				Log.d("Dutch Blitz", "Error: Player does not exist.");
				resTxtPlace = -1;
				resTxtScore = -1;
				resBtnGoScore = -1;
				break;
		}
		txtPlace = (TextView)findViewById(resTxtPlace);
		txtScore = (TextView)findViewById(resTxtScore);
		btnGoScore = (Button)findViewById(resBtnGoScore);
		
		// create a controller for the button
		final Context thisContext = context;
		final int thisPlayer = player;
		btnGoScore.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// tell the model which player it is
				MyApplication.model.setCurrentPlayer(thisPlayer);
				
				// create intent to request the score activity to be shown
				Intent intent = new Intent(thisContext, ScoreActivity.class);
				// start the activity
	            thisContext.startActivity(intent);
			}
		});
	}
	
	// update interface
	public void update(Observable observable, Object data){
		Log.d("Dutch Blitz", "update PlayerView");
		int place = MyApplication.model.getPlayerPlace(player);
		String strPlace = String.valueOf(place);
		switch (place){
			case -1:
				strPlace = "n/a";
				break;
			case 1:
				strPlace += "st";
				break;
			case 2:
				strPlace += "nd";
				break;
			case 3:
				strPlace += "rd";
				break;
			case 4:
				strPlace += "th";
				break;
		}
		txtPlace.setText(strPlace);
		txtScore.setText(String.valueOf(MyApplication.model.getPlayerScore(player)));
		btnGoScore.setText("Score\nRound " + String.valueOf(MyApplication.model.getPlayerRound(player)));
		if (MyApplication.model.getMinPlayerRound() == MyApplication.model.getPlayerRound(player)
				&& MyApplication.model.getPlayerActive(player)){
			btnGoScore.setEnabled(true);
		}
		else {
			btnGoScore.setEnabled(false);
		}
	}
}