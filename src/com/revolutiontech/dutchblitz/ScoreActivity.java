/*
 * Name: Lucas Connors
 * Project: Dutch Blitz Sidekick
 * File: ScoreActivity.java
 */

package com.revolutiontech.dutchblitz;

import java.util.Observable;
import java.util.Observer;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class ScoreActivity extends Activity implements Observer {
	// constants
	private static final int P_GREEN = 0;
	private static final int P_RED = 1;
	private static final int P_BLUE = 2;
	private static final int P_YELLOW = 3;
	
	// widgets
	private TextView txtRound;
	private EditText editTxtDutchPile;
	private EditText editTxtBlitzPile;
	private ImageView imgDutchPile;
	private ImageView imgBlitzPile;
	private TextView txtCalculatedDutchPile;
	private TextView txtCalculatedBlitzPile;
	private TextView txtCalculatedScore2;
	private Button btnCalculateScore;
	private Button btnSetScore;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_score);
		// Show the Up button in the action bar.
		setupActionBar();
		
		// grab widgets
		txtRound = (TextView)findViewById(R.id.txtRound);
		editTxtDutchPile = (EditText)findViewById(R.id.editTxtDutchPile);
		editTxtBlitzPile = (EditText)findViewById(R.id.editTxtBlitzPile);
		imgDutchPile = (ImageView)findViewById(R.id.imgDutchPile);
		imgBlitzPile = (ImageView)findViewById(R.id.imgBlitzPile);
		txtCalculatedDutchPile = (TextView)findViewById(R.id.txtCalculatedDutchPile);
		txtCalculatedBlitzPile = (TextView)findViewById(R.id.txtCalculatedBlitzPile);
		txtCalculatedScore2 = (TextView)findViewById(R.id.txtCalculatedScore2);
		btnCalculateScore = (Button)findViewById(R.id.btnCalculateScore);
		btnSetScore = (Button)findViewById(R.id.btnSetScore);
		
		// add this activity to the model's list of observers
        MyApplication.model.addObserver(this);
		
		// create a controller for the buttons
        final Context thisContext = this;
		btnCalculateScore.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// get values from piles
				int dutchPile = 0;
				int blitzPile = 0;
				try {
					dutchPile = Integer.valueOf(editTxtDutchPile.getText().toString());
				}
				catch (NumberFormatException e){
					Log.w("Dutch Blitz", "Dutch Pile is not a number- setting to 0");
				}
				try {
					blitzPile = Integer.valueOf(editTxtBlitzPile.getText().toString());
				}
				catch (NumberFormatException e){
					Log.w("Dutch Blitz", "Blitz Pile is not a number- setting to 0");
				}
				
				// set model values
				MyApplication.model.setPlayerDutchPile(dutchPile);
				MyApplication.model.setPlayerBlitzPile(blitzPile);
				
				// set calculated
				MyApplication.model.setCalculated(true);
				
				MyApplication.model.setChanged();
				MyApplication.model.notifyObservers();
			}
		});
		btnSetScore.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// calculate round score
				MyApplication.model.calculatePlayerRoundScore();
				// increase player round
				MyApplication.model.increasePlayerRound();
				// calculate player places
				MyApplication.model.calculatePlayerPlaces();
				// reset calculated
				MyApplication.model.setCalculated(false);
				
				// notify observers
				MyApplication.model.setChanged();
				MyApplication.model.notifyObservers();
				
				// move back to other activity
				Intent intent = new Intent(thisContext, MainActivity.class);
				// start the activity
	            thisContext.startActivity(intent); 
			}
		});
		
		// update
		MyApplication.model.setChanged();
		MyApplication.model.notifyObservers();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onDestroy() {
		Log.d("Dutch Blitz", "ScoreActivity: onDestroy");

		// remove view from the model's list when destroyed
		MyApplication.model.deleteObserver(this);
		
		super.onDestroy();
	}
	
	// update interface
	public void update(Observable observable, Object data){
		Log.d("Dutch Blitz", "update ScoreActivity");
		int round = MyApplication.model.getPlayerRound(MyApplication.model.getCurrentPlayer());
		int dutchPile = MyApplication.model.getPlayerDutchPile();
		int blitzPile = MyApplication.model.getPlayerBlitzPile();
		int roundScore = MyApplication.model.getPlayerRoundScore();
		txtRound.setText(getResources().getString(R.string.round) + " " + String.valueOf(round));
		if (editTxtDutchPile.getText().toString().equals("")){
			editTxtDutchPile.setHint(String.valueOf(dutchPile));
		}
		else {
			editTxtDutchPile.setText(String.valueOf(dutchPile));
		}
		if (editTxtBlitzPile.getText().toString().equals("")){
			editTxtBlitzPile.setHint(String.valueOf(blitzPile));
		}
		else {
			editTxtBlitzPile.setText(String.valueOf(blitzPile));
		}
		txtCalculatedDutchPile.setText(String.valueOf(dutchPile));
		txtCalculatedBlitzPile.setText(String.valueOf(blitzPile));
		txtCalculatedScore2.setText(getResources().getString(R.string.calculated_score2) + String.valueOf(roundScore));
		btnSetScore.setText(getResources().getString(R.string.set)+ " " + String.valueOf(roundScore)
				+ " " + getResources().getString(R.string.set_score) + " " + String.valueOf(round));
		
		// disable btnSetScore if not yet calculated
		if (MyApplication.model.getCalculated()){
			btnSetScore.setEnabled(true);
		}
		else {
			btnSetScore.setEnabled(false);
		}
		
		// display correct colour
		switch (MyApplication.model.getCurrentPlayer()){
			case P_GREEN:
				imgDutchPile.setImageResource(R.drawable.green);
				imgBlitzPile.setImageResource(R.drawable.green);
				break;
			case P_RED:
				imgDutchPile.setImageResource(R.drawable.red);
				imgBlitzPile.setImageResource(R.drawable.red);
				break;
			case P_BLUE:
				imgDutchPile.setImageResource(R.drawable.blue);
				imgBlitzPile.setImageResource(R.drawable.blue);
				break;
			case P_YELLOW:
				imgDutchPile.setImageResource(R.drawable.yellow);
				imgBlitzPile.setImageResource(R.drawable.yellow);
				break;
		}
	}
}