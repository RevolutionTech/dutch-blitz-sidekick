/*
 * Name: Lucas Connors
 * Project: Dutch Blitz Sidekick
 * File: MainActivity.java
 */

package com.revolutiontech.dutchblitz;

import com.crashlytics.android.Crashlytics;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.ViewGroup;


public class MainActivity extends Activity {
	// constants
	private static final int P_GREEN = 0;
	private static final int P_RED = 1;
	private static final int P_BLUE = 2;
	private static final int P_YELLOW = 3;
	
	// player views
	private PlayerView playerView[] = new PlayerView[4];
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Crashlytics.start(this);
		Log.d("Dutch Blitz", "Main onCreate");
        
        setContentView(R.layout.activity_main);
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState){
		super.onPostCreate(savedInstanceState);
		
		Log.d("Dutch Blitz", "Main onPostCreate");
		
		// create the views and add them to the main activity
		for (int i=0; i<4; ++i){
			playerView[i] = new PlayerView(this, i);
			int resPlayerView;
			switch (i){
				case P_GREEN:
					resPlayerView = R.id.main_activity_green;
					break;
				case P_RED:
					resPlayerView = R.id.main_activity_red;
					break;
				case P_BLUE:
					resPlayerView = R.id.main_activity_blue;
					break;
				case P_YELLOW:
					resPlayerView = R.id.main_activity_yellow;
					break;
				default:
					Log.d("Dutch Blitz", "Error: Player does not exist.");
					resPlayerView = -1;
					break;
			}
			ViewGroup v = (ViewGroup)findViewById(resPlayerView);
			v.addView(playerView[i]);
		}
		
		// initialize views
		MyApplication.model.setChanged();
		MyApplication.model.notifyObservers();
    }
    
    @Override
	protected void onDestroy() {
    	super.onDestroy();
    	
		Log.d("Dutch Blitz", "Main: onDestroy");

		// remove views from the model's list when destroyed
		for (int i=0; i<4; ++i){
			MyApplication.model.deleteObserver(playerView[i]);
		}
	}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	// Inflate the menu; this adds items to the action bar if it is present.
    	getMenuInflater().inflate(R.menu.main, menu);
    	
    	// get the menu item to add a listener
		MenuItem itemNewGame = menu.findItem(R.id.new_game);
		MenuItem itemChangePlayers = menu.findItem(R.id.change_players);
		MenuItem itemAbout = menu.findItem(R.id.about);
		
		// set context as final to use in anonymous class
		final Context thisContext = this;
		
		// create the menu item controllers 
		itemNewGame.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				// reset the model
				MyApplication.model.reset();
	
	            return true;
			}
		});
		itemChangePlayers.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				// copy active players list
				final boolean playerActive[] = new boolean [4];			// the final is a lie
				for (int i=0; i<4; ++i){
					playerActive[i] = MyApplication.model.getPlayerActive(i);
				}
				
				// show alert with ask the active players
				AlertDialog.Builder alert = new AlertDialog.Builder(thisContext);
			    alert.setTitle(R.string.change_players);
			    alert.setMultiChoiceItems(R.array.player_colors, playerActive,
		    		new DialogInterface.OnMultiChoiceClickListener() {
	                	@Override
	                	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
		                    if (isChecked) {
		                    	playerActive[which] = true;
		                    } else {
		                    	playerActive[which] = false;
		                    }
	                	}
	            	}
			    );
			    alert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) {
			        	// if all players inactive, set them all active
			        	Boolean allInActive = true;
			        	for (int i=0; i<4; ++i){
			        		if (playerActive[i]){
			        			allInActive = false;
			        			break;
			        		}
			        	}
			        	if (allInActive){
			        		for (int i=0; i<4; ++i){
			        			playerActive[i] = true;
			        		}
			        	}
			        	
			            // update the active players
			        	for (int i=0; i<4; ++i){
			        		MyApplication.model.setPlayerActive(i, playerActive[i]);
			        	}
			        	MyApplication.model.calculatePlayerPlaces();
			        	
			        	// notify observers
			        	MyApplication.model.setChanged();
			        	MyApplication.model.notifyObservers();
			        }
			    });
			    alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // do nothing, just close the dialog
			        	dialog.cancel();
			        }
			    });
			    alert.show();
				
	            return true;
			}
		});
		itemAbout.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				// show alert with about information
				AlertDialog.Builder alert = new AlertDialog.Builder(thisContext);
			    alert.setTitle(R.string.about);
			    alert.setMessage(R.string.about_desc);
			    alert.setPositiveButton(R.string.about_ok, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // do nothing, just close the dialog
			        	dialog.cancel();
			        }
			    });
			    alert.show();
				
	            return true;
			}
		});
    	
        return true;
    }
}