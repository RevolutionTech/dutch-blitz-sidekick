/*
 * Name: Lucas Connors
 * Project: Dutch Blitz Sidekick
 * File: MyApplication.java
 */

package com.revolutiontech.dutchblitz;

import android.app.Application;

public class MyApplication extends Application {
	// an application-wide reference to the model
    public static Model model = new Model();
}