# Dutch Blitz Sidekick
# by Revolution Software

Design: Adam Machan

Code: Lucas Connors

An app made for the card game Dutch Blitz

![Dutch Blitz Sidekick](http://revolutiontech.ca/media/img/dutch-blitz-sidekick.jpg)